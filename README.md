# Technical Task for Reign

Reign Technical Task, to apply for the position of Junior Back End Developer.

## Developer

- Albert Acevedo

### Technical specifications / References

- Official documentation `https://expressjs.com/`

#### Prerequisites:

- NodeJS
- Npm
- Yarn (Optional)

#### Branch

- main -> Production `https://reign-task.herokuapp.com/api/v1`

### Test Swagger

```
https://reign-task.herokuapp.com/swagger/
```

#### Commands for displaying functions.

| °   | Command                         | Description                                    |
| --- | ------------------------------- | ---------------------------------------------- |
| 1   | `npm run dev` or `yarn dev`     | Compilation of the application for development |
| 2   | `npm run build` or `yarn build` | Construction of the production application     |
| 3   | `npm run start` or `yarn start` | Running the application in production          |

### Folder Structure

```
├──src:
|   ├──bin:
|   │   ├── www.js
|   ├──components:
|   │   ├── ...
|   ├──config:
|   │   ├── message.js
|   ├──microservices:
|   │   ├── ...
|   ├──middleware:
|   │   ├── ...
|   ├──routes:
|   │   ├── index.js
|   ├──services:
|   │   ├── ...
|   ├──router:
|   │   ├── ...
|   ├──services:
|   │   ├── ...
|   ├──app.js
├──.babelrc
├──.editorconfig
├──.eslintrc.json
├──.gitignore
├──.gitlab-ci.yml
├──.gitignore
├──jsconfig.json
├──prettier.config.js
├──package.json
├──package-lock.json
├──README.md
```

### Notes

- The application was created by means of express and documented in swagger.
- To make use of the project you must first configure the environment variable MONGODB_CNN with your connection link to the MongoDB cluster. Example:

```
MONGODB_CNN=mongodb+srv://username:password@cluster0-jtpxd.mongodo.net/admin
```

- Packages and dependencies used for processing:

| °   | Package                | Version   |
| --- | ---------------------- | --------- |
| 1   | `axios`                | `^0.25.0` |
| 2   | `cors`                 | `^2.8.5`  |
| 3   | `dotenv`               | `^10.0.0` |
| 4   | `express`              | `^4.17.1` |
| 5   | `express-ip`           | `^1.0.4`  |
| 6   | `express-validator`    | `^6.14.0` |
| 7   | `helmet`               | `^5.0.2`  |
| 8   | `mongoose`             | `^6.2.0`  |
| 9   | `mongoose-paginate-v2` | `^1.6.0`  |
| 10  | `node-cron`            | `^3.0.0`  |
| 11  | `swagger-jsdoc`        | `^6.1.0`  |
| 12  | `swagger-ui-express`   | `^4.3.0`  |
