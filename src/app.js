import express from 'express'
import cors from 'cors'
import swaggerUI from 'swagger-ui-express'
import helmet from 'helmet'
import expressip from 'express-ip'
import { swaggerDocs } from './services/swagger'

// import routes
import routes from './routes'

const app = express()

// middleware
app.use(helmet())
app.use(cors())
app.use(expressip().getIpInfoMiddleware)
app.use(express.json())
app.use('/api/v1', routes)
app.use('/swagger', swaggerUI.serve, swaggerUI.setup(swaggerDocs))

export default app
