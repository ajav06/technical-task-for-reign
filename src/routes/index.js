import { articleRoutes } from '@components/article/routes'
import { Router } from 'express'

const router = Router()

router.use('/article', articleRoutes)

export default router
