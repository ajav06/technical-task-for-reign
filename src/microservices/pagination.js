export const getPaginationQuerys = (query) => {
  let author = query.author || null
  let title = query.title || null
  let tag = query.tag || null
  let page = query.page && query.page > 1 ? parseInt(query.page) - 1 : 0
  let size = parseInt(query.size) || 5

  return { author, title, tag, page, size }
}

export const getPagination = (page, size) => {
  const limit = size ? +size : 5
  const offset = page ? page * limit : 0

  return { limit, offset }
}

export const getPagingData = (data) => {
  const totalItems = data.totalDocs || data.length || 0
  const currentPage = data.page || 1
  const totalPages = data.totalPages || 0
  const items = data.docs || data

  return { totalItems, currentPage, totalPages, items }
}
