import { addArticle } from '@components/article/dao'
import axios from 'axios'
import { schedule } from 'node-cron'

export const capitalize = (str = '') => {
  return str.charAt(0) + str.slice(1).toLowerCase()
}

const addArticles = async () => {
  try {
    const { data } = await axios.get(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs'
    )
    for (const article of data.hits) {
      await addArticle(article)
    }

    console.log('Articles successfully added!')
  } catch (error) {
    console.log(error)
  }
}

export const autoAddArticle = async () =>
  schedule('0 */1 * * *', async () => {
    await addArticles()
  }).start()
