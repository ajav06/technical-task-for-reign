import { model, Schema } from 'mongoose'
import mongoosePaginate from 'mongoose-paginate-v2'

const ArticleSchema = new Schema({
  created_at: {
    type: 'Date'
  },
  title: {
    type: 'String'
  },
  url: {
    type: 'Mixed'
  },
  author: {
    type: 'String'
  },
  points: {
    type: 'Mixed'
  },
  story_text: {
    type: 'Mixed'
  },
  comment_text: {
    type: 'String'
  },
  num_comments: {
    type: 'Mixed'
  },
  story_id: {
    type: 'Number'
  },
  story_title: {
    type: 'String'
  },
  story_url: {
    type: 'String'
  },
  parent_id: {
    type: 'Number'
  },
  created_at_i: {
    type: 'Number'
  },
  _tags: {
    type: ['String']
  },
  objectID: {
    type: 'String'
  },
  _highlightResult: {
    author: {
      value: {
        type: 'String'
      },
      matchLevel: {
        type: 'String'
      },
      matchedWords: {
        type: ['String']
      }
    },
    comment_text: {
      value: {
        type: 'String'
      },
      matchLevel: {
        type: 'String'
      },
      fullyHighlighted: {
        type: 'Boolean'
      },
      matchedWords: {
        type: ['String']
      }
    },
    story_title: {
      value: {
        type: 'String'
      },
      matchLevel: {
        type: 'String'
      },
      matchedWords: {
        type: ['String']
      }
    },
    story_url: {
      value: {
        type: 'String'
      },
      matchLevel: {
        type: 'String'
      },
      matchedWords: {
        type: ['String']
      }
    }
  }
})

ArticleSchema.method('toJSON', function () {
  const { __v, _id, ...object } = this.toObject()
  return object
})

ArticleSchema.plugin(mongoosePaginate)

export const ArticleModel = model('Article', ArticleSchema)
