import { param, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import { findArticleById } from '../dao'

export const articleExistRequest = async (req, res, next) => {
  try {
    await param('id', `El id ${message.request}`).notEmpty().run(req)

    const request = validationResult(req)

    if (!request.isEmpty()) return handleResponse(res, 422, request)

    const article = await findArticleById(req.params.id)

    if (!article) {
      handleResponse(res, 404, message.not_found_long)
    } else {
      next()
    }
  } catch (error) {
    handleError(error.toString(), res)
  }
}
