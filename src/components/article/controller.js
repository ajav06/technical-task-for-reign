import { message } from '@config/message'
import {
  getPagination,
  getPaginationQuerys,
  getPagingData
} from '@microservices/pagination'
import { handleResponse } from '@middleware/errorHandlers'
import { handleError } from '@middleware/errorHandlers'
import { deleteArticleById, findArticles } from './dao'

export const getArticles = async (req, res) => {
  try {
    const { author, tag, title, page, size } = getPaginationQuerys(req.query)

    const { limit, offset } = getPagination(page, size)
    const result = await findArticles({ author, tag, title, limit, offset })
    const data = getPagingData(result)

    handleResponse(res, 200, message.accepted, data)
  } catch (error) {
    handleError(error, res)
  }
}

export const deleteArticle = async (req, res) => {
  try {
    await deleteArticleById(req.params?.id)

    handleResponse(res, 200, message.delete)
  } catch (error) {
    handleError(error, res)
  }
}
