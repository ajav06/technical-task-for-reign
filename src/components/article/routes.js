import { Router } from 'express'
import { deleteArticle, getArticles } from './controller'
import { articleExistRequest } from './middleware/validator'

const router = Router()

router.get('/', getArticles)
router.delete('/:id', articleExistRequest, deleteArticle)

export const articleRoutes = router
