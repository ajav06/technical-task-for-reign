import { ArticleModel } from './model'

export const addArticle = async (article) => {
  try {
    return await ArticleModel.create(article)
  } catch (error) {
    throw error
  }
}

export const findArticleById = async (objectID) => {
  try {
    return await ArticleModel.findOne({ objectID })
  } catch (error) {
    throw error
  }
}

export const findArticles = async ({ author, title, tag, limit, offset }) => {
  try {
    const condition =
      author || title || tag
        ? {
            $and: [
              author
                ? { author: { $regex: new RegExp(author), $options: 'i' } }
                : {},
              title
                ? { title: { $regex: new RegExp(title), $options: 'i' } }
                : {},
              tag ? { _tags: { $regex: new RegExp(tag), $options: 'i' } } : {}
            ]
          }
        : {}

    return await ArticleModel.paginate(condition, { offset, limit })
  } catch (error) {
    throw error
  }
}

export const deleteArticleById = async (objectID) => {
  try {
    return await ArticleModel.findOneAndDelete({ objectID })
  } catch (error) {
    throw error
  }
}
