import mongoose from 'mongoose'
require('dotenv').config()

const mongoDB = async () => {
  try {
    await mongoose.connect(process.env.MONGODB_CNN, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    console.log('connected to mongoDB')
  } catch (error) {
    console.error(error)
  }
}

mongoDB()
