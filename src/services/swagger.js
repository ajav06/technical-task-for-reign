import swaggerJsonDoc from 'swagger-jsdoc'

const swaggerOptions = {
  openapi: '3.0.0',
  swaggerDefinition: {
    info: {
      title: "Technical Task for Reign REST API's",
      description:
        'Reign technical task, to apply for the position of Junior Back End Developer (Albert Acevedo).',
      version: '1.0.0'
    },
    basePath: '/api'
  },
  apis: ['./src/components/*/*.yml']
}

export const swaggerDocs = swaggerJsonDoc(swaggerOptions)
