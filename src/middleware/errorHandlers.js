import { validationResult } from 'express-validator'

export const handleError = (error, res) => {
  res.status(500).json({
    status: 'error',
    message: 'SERVER ERROR',
    error: error.toString()
  })
}

export const handleValidationResult = (req, res) => {
  const request = validationResult(req)
  if (!request.isEmpty()) return res.status(422).json(request)
}

export const handleResponse = (res, status, message_resp, data = {}) => {
  res.status(status).json({
    message: message_resp,
    data: data
  })
}
